section .data
    aste db 10,'*'
    ;len equ $-aste
    new_line db 10,''
section .text
    global _start
_start:
    mov ecx, 9
    mov ebx,9
ciclo1:
    push   ebx
    cmp ebx,'0'
    jz salir
    jmp ciclo2
ciclo2:
    dec ecx
    push cx
    ciclo1 aste
    pop cx
    cmp cx,0
    jg aste
    imprimir new_line
    pop ebx
    dec ebx
    mov ecx,9; mov ecx,ebx
    jmp ciclo1
salir:
    mov eax,1
    int 80h

