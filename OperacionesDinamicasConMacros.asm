;Miguel Rojas
;Realizar las operaciones bassicas utilizando la etiqueta macro
;que las operaciones reciban datos por teclado
%macro imprimir 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro leer 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
   
    mensaje1 db 'Ingrese el primer número:'
    len1 equ $ - mensaje1
    mensaje2 db 'Ingrese el segundo número:'
    len2 equ $ - mensaje2
    datoSuma db 10, 'La suma es: '
    longSuma equ $ - datoSuma
    datoResta db 10,'La resta es: '
    longResta equ $ - datoResta
    datoProducto db 10,'El producto es: '
    longProducto equ $ - datoProducto
    datoDivision db 10, 'El cociente de la división es: '
    longDivision equ $ - datoDivision
    datoResiduo db 10,'El residuo de la división es: '
    longResiduo equ $ - datoResiduo
   
section .bss
    numero resb 1
    numero1 resb 1
    suma resb 1
    resta resb 1
    producto resb 1
    cociente resb 1
    residuo resb 1

section .text
    global _start
    _start:
      
        imprimir mensaje1, len1
        leer numero, 2; soon 2 espacios

        imprimir mensaje2, len2
        leer numero1, 2
        ;SUMA
        mov al, [numero]
        mov bl, [numero1]
        sub ax, '0'
        sub bl, '0'
        add al, bl
        add al, '0'
        mov [suma], al

        ;RESTA
        mov al, [numero]
        mov bl, [numero1]
        sub ax, '0'
        sub bl, '0'
        sub al, bl
        add al, '0'
        mov [resta], al

        ;MULTIPLICACIÓN
        mov al, [numero]
        mov bl, [numero1]
        sub ax, '0'
        sub bl, '0'
        mul bl
        add al, '0'
        mov [producto], al

        ; Division
        mov al, [numero]
        mov bl, [numero1]
        sub ax, '0'
        sub bl, '0'
        div bl
        add al, '0'
        add ah, '0'  
        mov [cociente], al
        mov [residuo], ah

        imprimir datoSuma, longSuma
        imprimir suma, 1
        imprimir datoResta, longResta
        imprimir resta , 1
        imprimir datoProducto, longProducto
        imprimir producto, 1
        imprimir datoDivision, longDivision
        imprimir cociente, 1
        imprimir datoResiduo, longResiduo
        imprimir residuo, 1
        mov eax, 1
        int 80h