%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj db "Ingrese 5 numeros: ", 10
    len equ $ - msj

    msj1 db 10, "El number mayor es: ", 10
    len1 equ $ - msj1

    array db 0,0,0,0,0
    len_array equ $-array 

section .bss
    number resb 2
    mayor resb 2

section .text
    global _start

_start:
    print msj, len
    
    mov esi, array
    mov edi, 0

read_numbers:
    read number, 2
    mov al, [number]
    sub al, '0'
    mov [esi], al

    add esi, 1
    add edi, 1

    cmp edi, len_array
    je input
    jb read_numbers

input:
    mov esi, array
    mov edi, 0
    mov al, [esi]
    mov [number], al
    mov cx, 0

    jmp compare

compare:
    
    mov al, [number]
    add esi, 1
    add edi, 1
    mov bl, [esi]
    cmp al, bl
    jg cmp_mayor 
    je cmp_mayor
    jmp cmp_menor

cmp_mayor: 
    cmp edi, len_array
    je print_result
    jmp compare

cmp_menor: 
    mov [number], bl
    cmp edi, len_array
    je print_result
    jmp compare

print_result:
    mov al, [number]
    add al, '0'
    mov [mayor], al
    print msj1, len1
    print mayor, 1

salir:
    mov eax, 1
    int 80h
