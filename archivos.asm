%macro write 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj1 db "ingresa datos del archivo",10
    len_msj equ $ -msj1
    archivo db "/home/alex/Música/archivo.txt"
section .bss
    texto resb 30
    idarchivo resb 1
section .text
    global _start
lectura:
    mov eax, 3
    mov ebx, 0
    mov ecx, texto
    mov edx, 10
    int 80h
    ret
_start:
; **********crear el archivo**********
   ;O-ROONLY 0: se abre solo para lectura
   ;O-WRONLY 1: se abre solo para escritura
   ;O-ROWR   2: se abre para lectura y escritura
   ;O-CREATE 256: crea en caso de que no existe
   ;O-APPEND 2000h: se abre para escritura al final
    mov eax,8 ; servicio para trabajar archivos
    mov ebx,archivo
    mov ecx, 1;modo de acceso
    mov edx, 777h ;permisos 
    int 80h

    test eax, eax;solo afecta a registro de estados
    jz salir
            ;TEST reg,reg
            ;TEST reg,men
            ;TEST men,reg
            ;TEST reg,inmediato
            ;TEST men,inmediato
    mov dword [idarchivo], eax
    write msj1,len_msj

    
    call lectura
;________________Escribir en archivo____________________________________________
    mov eax, 4
    mov ebx, [idarchivo]
    mov ecx, texto
    mov edx, 10
    int 80h

salir:
    mov eax,1
    int 80h
