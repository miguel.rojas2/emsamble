;Miguel Alexander Rojas Cobos, 29 julio 2020
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    aste db '*'
    ente db "",10
    len_enter equ $-ente

section .text
    global _start
_start:
    mov ebx, 9  ; 
    mov ecx, 9  ; 
inicio: ;principal
    push bx ;
    cmp bx, 0
    jz salir
    jmp asterisco
asterisco:
    push cx
    imprimir aste,1    
    pop cx
    loop asterisco
    imprimir ente,len_enter  
    pop bx 
    dec ebx
    mov ecx, ebx; aqui se cambia el valro
    loop inicio

salir:
    mov eax,1
    int 80h  
    
