%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj db 10,'Hola '
    len equ $ - msj

section .bss
    n resb 1

section .text
    global _start

_start:
    mov ecx, 9

imprimir:
    push cx
    add cx, '0'
    mov [n], cx
    print msj, len
    print n, 2

    pop cx
    loop imprimir

salir:
    mov eax, 1
    int 80h
