%macro write 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj1 db "lectura datos del archivo",10
    len_msj equ $ -msj1
    archivo db "/home/alex/Música/archivo.txt"
section .bss
    texto resb 30
    idarchivo resb 1
section .text
    global _start

_start:
; **********crear el archivo**********
   ;O-ROONLY 0: se abre solo para lectura
   ;O-WRONLY 1: se abre solo para escritura
   ;O-ROWR   2: se abre para lectura y escritura
   ;O-CREATE 256: crea en caso de que no existe
   ;O-APPEND 2000h: se abre para escritura al final
    mov eax,5 ; servicio para trabajar archivos
    mov ebx,archivo
    mov ecx, 0;modo de acceso
    mov edx, 0 ;permisos 
    int 80h

    test eax, eax;solo afecta a registro de estados
    jz salir
            ;TEST reg,reg
            ;TEST reg,men
            ;TEST men,reg
            ;TEST reg,inmediato
            ;TEST men,inmediato
    mov dword [idarchivo], eax
    write msj1,len_msj

;________________Leer archivo_______________________________________________
    mov eax, 3
    mov ebx, [idarchivo]
    mov ecx, texto
    mov edx, 15;maximo caracteres a leer
    int 80h 
    write texto,15

salir:
    mov eax,1
    int 80h
