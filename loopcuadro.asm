;Miguel Alexander Rojas Cobos, 29 julio 2020
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    aste db '*'
    ente db "",10
    len_enter equ $-ente

section .text
    global _start
_start:
    mov ebx, 9   
    mov ecx, 9  
inicio: 
    push ebx 
    cmp ebx, 0
    jz salir
    jmp asterisco
asterisco:
    push ecx
    imprimir aste,1    
    pop ecx
    loop asterisco
    imprimir ente,len_enter  
    pop ebx 
    dec ebx
    mov ecx, 9; en este parte se camba el 9 por ebx y obtenemos la piramide
    ;mov ecx,ebx  : esta linea tranforma a una piramide, el cuadrado que formamos
    loop inicio

salir:
    mov eax,1
    int 80hh
