%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
section .data

	mensaje db "a * b = c ",10
	len_mensaje equ $ - mensaje
	
	m db " * "
	lenm equ $-m
	
	migual db " = "
	leni equ $-migual
	
	msj4 db " |-------| "
	len4 equ $-msj4
	
	new_line db 10
	len_esp equ $-new_line
	
section .bss
	n rest 1
	pre rest 1
	pre2 rest 1
	re rest 1

section .text
	global _start
	
__start:
	
	mov al, 1
	mov [re], al
	mov ecx, 0
	
inicio:
	cmp ecx, 8
	jz ciclo
	inc ecx
	push ecx
	mov [n], ecx
	jmp imp
	
imp:
	mov al, [n]
	mov [pre], al
	mov cl, [re]
	mul cl
	mov [n], al
	mov ah, [pre]
	add ax, '00'
	add cx, '00'
	mov [pre], ah
	mov [pre2], cl
	call imprimir_numero
	imprimir m,lenm
	call imprimir_numero2
	imprimir migual,leni
	mov eax, 48
	add [n], eax 
	call imprimir_n
	imprimir msj4, len4
	pop ecx
	jmp inicio
imprimir_numero:
	mov eax, 4
	mov ebx, 1
	mov ecx, pre
	mov edx, 1
	int 80h
	ret
imprimir_numero2:
	mov eax, 4
	mov ebx, 1
	mov ecx, pre2
	mov edx, 1
	int 80h
	ret
imprimir_n:
	mov eax, 4
	mov ebx, 1
	mov ecx, n
	mov edx, 1
	int 80h
	ret
imprimir_m:
    mov eax, 4
	mov ebx, 1
	mov ecx, m
	mov edx, 1
	int 80h
	ret
imprimir_i:
    mov eax, 4
	mov ebx, 1
	mov ecx, migual
	mov edx, 1
	int 80h
	ret
ciclo: 
	imprimir new_line, len_esp
	mov ebx, [re]
	inc ebx
	mov [re], ebx
	mov ecx, 0
	cmp ebx, 10
	jb inicio
	
salir:
	mov eax, 1
	mov ebx, 0
	int 80h