section .data
    salto db 10,''
    len_salto equ $-salto
section .bss
    num_a resb 2
section .text
    global _start

_start:
    mov ebx, 0
    mov edx, 1

    add ebx, '0'
    add edx, '0'

    mov [num_a], ebx ; numb_a = 0
    

    call imprimir_numero ; 0
  
    mov ecx, 6

    jmp multiplicar

imprimir_numero:
    mov eax, 4
    mov ebx, 1
    mov ecx, num_a
    mov edx, 1
    int 80h

    ret

salida:
    mov eax, 1
    int 80h
